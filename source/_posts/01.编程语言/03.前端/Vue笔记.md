---
title: Vue笔记
comments: true
tags:
  - 前端
  - Vue
categories:
  - 01.编程语言
  - 03.前端
date: 2017-09-08 21:21:21
---
## 1.安装

- vue
- vue-cli
- vue-devtools

## 2.Vue实例生命周期

![Vue实例声明周期](/images/posts/vue-lifecycle.png)